#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import rospy
import preference_manager
from lovot_internal_state.msg import *

# globals
pref_manager = None

# constants
MAX_UNITSIZE = 10
DECAY_RATE = 0.5

# 時間セグメント、24h表記0始まり24終わりを前提。
# 開始値は必ずゼロなので、第二セグメント以降の開始値を記載
TIME_SEGMENTS_ONE = []
TIME_SEGMENTS_THREE = [8, 16]
TIME_SEGMENTS_FIVE = [4, 10, 14, 20]
TIME_SEGMENTS_SEVEN = [3, 7, 10, 14, 17, 21]
TIME_SEGMENTS_NINE = [5, 7, 9, 11, 13, 15, 17, 19]
TIME_SEGMENTS_ELEVEN = [3, 5, 7, 9, 11, 13, 15, 17, 19, 21]

# ガウス分布の最小値（中央値から12時間たったところでの値）
MIN_ABS_PREFERENCE = 0.2

# callback
def human_curiosity_callback(data):
    u"""ある程度の頻度出現しているものについて、長期記憶に留める"""
    global pref_manager
    for unit in data.curiosity_objects:
        pref_manager.update_object(unit.name, unit.location.x, unit.location.y)
    pref_manager.publish_preference_list()
    
# main function
def preference_map_node():
    global pref_manager
    
    # init node
    rospy.init_node('preference_map_node')
    # initialize curiosity manager
    pref_manager = preference_manager.PreferenceManager('human',
                                                        MAX_UNITSIZE,
                                                        DECAY_RATE,
                                                        TIME_SEGMENTS_FIVE,
                                                        MIN_ABS_PREFERENCE
                                                        )
    # Subscriber
    rospy.Subscriber('/lovot_curiosity/human/filtered_curiosity_list',
                     CuriosityObjectList, human_curiosity_callback)
    
    # spin
    rospy.spin()

# Setup the application
if __name__ == '__main__':
    try:
        preference_map_node()
    except rospy.ROSInterruptException:
        pass

#!/usr/bin/env python
# -*- coding: utf-8 -*-
import rospy
import math
import collections
from datetime import datetime
from lovot_preference_map.msg import *
from geometry_msgs.msg import Vector3

# class definitions
class TimeUnitWithPreference:
    u"""選好度保有時間ユニットクラス"""
    def __init__(self, index, start, end, min_preference):
        u"""初期化"""
        self.index = index
        self.start = start
        self.end = end
        self.pgain_coef = 0.0
        # 中央値から12時間ずれたところが最小値(必ず1未満）
        if min_preference >= 1.0:
            min_prefernece = 0.99
        self.pwidth_coef = math.sqrt(-72/math.log(min_preference))

    def update_prefernce(self, delta):
        u"""選好度の更新。値域は-1 <= prefernce <= 1"""
        self.pgain_coef += delta
        if self.pgain_coef > 1.0:
            self.pgain_coef = 1.0
        elif self.pgain_coef < -1.0:
            self.pgain_coef = -1.0

    def get_unittime(self):
        u"""代表値（中央時間）を返す"""
        return (self.start + self.end) / 2.0

    def get_preference(self, difftime):
        u"""中央時間からdifftimeずれた位置での選好度を返す"""
        pcoef = -1 * math.pow(difftime, 2) / math.pow(self.pwidth_coef, 2)
        return self.pgain_coef * math.exp(pcoef)
            
    u"""以下、アクセッサ"""            
    def get_index(self):
        return self.index

    def get_starttime(self):
        return self.start

    def get_endtime(self):
        return self.end    

class TimeSegmentsWithPreference:
    u"""時間セグメントクラス"""
    def __init__(self, timesegments, minimum_abs_preference):
        u"""初期化"""
        self._timiunits = []
        timesegments_list = [0]
        timesegments_list.extend(timesegments)
        for i in range(len(timesegments_list)):
            tstart = timesegments_list[i]
            if i == len(timesegments_list) - 1:
                tend = 24
            else:
                tend = timesegments_list[i+1]
                
            self._timiunits.append(TimeUnitWithPreference(len(self._timiunits), tstart, tend, minimum_abs_preference))
            
    u"""以下、アクセッサ"""
    def get_unittime(self, index):
        return self._timiunits[index].get_unittime()

    def get_preference(self, index, difftime):
        return self._timiunits[index].get_preference(difftime)

    def get_starttime(self, index):
        return self._timiunits[index].get_starttime()

    def get_endtime(self, index):
        return self._timiunits[index].get_endtime()

    def get_segment_num(self):
        return len(self._timiunits)

    def update_prefernce(self, index, delta):
        self.timiunits[index].update_prefernce(delta)
        
class PreferenceModel:
    u"""個別の選好性クラス、時間方向のガウス分布の重ねあわせで選好性を表現する"""
    def __init__(self, name, initx, inity, decay_rate, timesegments, minimum_abs_preference):
        u"""初期化"""
        self.name = name
        self.x = initx
        self.y = inity
        self._time_segments = TimeSegmentsWithPreference(timesegments, minimum_abs_preference)
        self._decay_rate = decay_rate

    def update_location(self, curx, cury):
        u"""位置の更新"""
        self.x = curx
        self.y = cury

    def update_preference(self, delta):
        u"""時間セグメントの各時間ユニットごとの選好度更新"""
        check_time = int(datetime.now().strftime("%H")) + int(datetime.now().strftime("%M")) / 60.0
        center_index = -1
        for i in range(self._time_segments.get_segment_num()):
            if self._time_segments.get_starttime(i) <= check_time and self._time_segments.get_endtime(i) >= check_time:
                center_index = i

        if center_index >= 0:
            # 現在時刻のタイムユニットでの更新
            self._time_segments.update_prefernce(center_index, delta)
            loop_num = (self._time_segments.get_segment_num() - 1) / 2
            for j in range(loop_num):
                # 更新値の計算
                tdelta = delta * math.pow(self._decay_rate, (j + 1))
                
                # 現在のタイムユニットよりも前の時間セグメントの波及更新
                pindex = center_index - (j + 1)
                if pindex < 0:
                    pindex += self._time_segments.get_segment_num()                    
                self._time_segments.update_prefernce(pindex, tdelta)

                # 現在のタイムユニットよりも後の時間セグメントの波及更新                
                nindex = center_index + (j + 1)
                if nindex > self._time_segments.get_segment_num():
                    nindex -= self._time_segments.get_segment_num()
                self._time_segments.update_prefernce(nindex, tdelta)

    def get_preference(self, difftime):
        u"""現在時間からの差分を指定し、重ね合わせの結果を返す。重ねあわせとして絶対値のもっとも大きい値を返す"""
        check_time = int(datetime.now().strftime("%H")) + int(datetime.now().strftime("%M")) / 60.0 + difftime
        # 0.5刻みになるように調整
        check_time = int(check_time * 2) / 2.0
        
        pref = 0.0
        for i in range(self._time_segments.get_segment_num()):
            unittime = self._time_segments.get_unittime(i)
            tmppref = self._time_segments.get_preference(i, (check_time - unittime))
            if abs(tmppref) > pref:
                pref = tmppref
            
        return pref

    def get_max_preference(self):
        u"""全ての時間セグメントを通して絶対値が最大となる選好度を返す"""
        max_pref = 0.0
        for i in range(self._time_segments.get_segment_num()):
            segment_pref = self._time_segments.get_preference(i, 0)
            if abs(segment_pref) > max_pref:
                max_pref = segment_pref
        return max_pref
                        
    u"""以下、アクセッサ"""
    def get_name(self):
        return self.name

    def get_location(self):
        return (self.x, self.y)
                    
class PreferenceManager:
    u"""選好性管理クラス"""
    def __init__(self, name, maxunit, decayrate, timesegments, minimum_abs_preference):
        u"""初期化"""
        self.name = name
        self._maxunitsize = maxunit
        self._unit_array = collections.deque(maxlen=self._maxunitsize)
        self._decay_rate = decayrate
        self._timesegments = timesegments
        self._minimum_abs_preference = minimum_abs_preference
        pubname = '/lovot_preference/' + self.name + '/preference_list'
        self._preference_list_pub = rospy.Publisher(pubname, PreferenceInfoList, queue_size=10)        

    def delete_nth_unit(self, n):
        u"""n番目の要素を削除する"""
        self._unit_array.rotate(-n)
        self._unit_array.popleft()
        self._unit_array.rotate(n)

    def delete_unit_by_name(self, name):
        u"""名前で指定したユニットを削除するメソッド"""
        cnt = 0
        for unit in self._unit_array:
            if name == unit.get_name():
                delete_nth_unit(cnt)
                return
            else:
                ctn += 1
        print 'No such unit!'

    def delete_lowest_preference_unit(self):
        u"""全体的に最も選好度の絶対値が低いユニットを一つ削除するメソッド"""        
        preference_list = [x.get_max_preference() for x in self._unit_array]
        minval = min(preference_list)
        cnt = 0
        for unit in self._unit_array:
            if minval == unit.get_max_preference():
                delete_nth_unit(cnt)
                return
            else:
                ctn += 1

    def publish_preference_list(self):
        u"""ソート済み選好度リストをpublishするメソッド"""
        slist = sorted(self._unit_array, key=lambda x: x.get_preference(0), reverse=True)

        plist = PreferenceInfoList()
        for u in slist:
            pinfo = PreferenceInfo()
            pinfo.name = u.get_name()
            pinfo.location = Vector3()
            loc = u.get_location()
            pinfo.location.x = loc[0]
            pinfo.location.y = loc[1]
            pinfo.location.z = 0.0
            pinfo.preference = u.get_preference(0)
            plist.preference_infos.append(pinfo)

        if len(plist.preference_infos) > 0:
            self._preference_list_pub.publish(plist)
                        
    def update_object(self, name, posx, posy):
        u"""オブジェクトの更新"""
        unit_names = [x.get_name() for x in self._unit_array]
        
        if name in unit_names:
            unit = [x for x in self._unit_array if x.get_name() == name][0]
            unit.update_location(posx, posy)
        else:
            if len(self._unit_array) == self._maxunitsize:
                # 最大個数を超えている場合、選好度の絶対値が一番小さなものを削除する
                self.delete_lowest_preference_unit()
            self._unit_array.append(PreferenceModel(name, posx, posy,
                                                    self._decay_rate,
                                                    self._timesegments,
                                                    self._minimum_abs_preference))

    def update_prefernce_by_name(self, name, delta):
        u"""名前で指定したオブジェクトの選好度を更新する"""
        for unit in self._unit_array:
            if name == unit.get_name():
                unit.update_preference(delta)
                return
        print 'No such unit!'
        

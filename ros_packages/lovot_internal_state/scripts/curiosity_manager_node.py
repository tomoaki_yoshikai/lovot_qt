#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import rospy
import curiosity_manager
from lovot_people_perception.msg import *

# globals
human_curiosity_manager = None

# constants
UPDATE_TICK = 0.2
MAX_UNITSIZE = 10
CURIOSITY_WINDOWSIZE = 60
UNITDELETE_THRES = 0.1
MEMORIZE_THRES = 0.6
FACE_CONFIDENT_THRES = 0.5

# callback
def face_detect_callback(data):
    global human_curiosity_manager
    if data.confidence > FACE_CONFIDENT_THRES:
        human_curiosity_manager.update_object(data.scores[0].person_name, data.position.x, data.position.y) 

# main function
def curiosity_manager_node():
    global human_curiosity_manager
    
    # init node
    rospy.init_node('curiosity_manager_node')
        
    # initialize curiosity manager
    human_curiosity_manager = curiosity_manager.CuriosityManager('human',
                                                                 UPDATE_TICK,
                                                                 MAX_UNITSIZE,
                                                                 CURIOSITY_WINDOWSIZE,
                                                                 UNITDELETE_THRES,
                                                                 MEMORIZE_THRES)

    # setup shutdown hook
    rospy.on_shutdown(human_curiosity_manager.shutdown)
        
    # Subscriber
    rospy.Subscriber("/lovot_people_perception/face/person", FaceIdentInfo, face_detect_callback)
    
    # spin
    rospy.spin()

# Setup the application
if __name__ == '__main__':
    try:
        curiosity_manager_node()
    except rospy.ROSInterruptException:
        pass
        

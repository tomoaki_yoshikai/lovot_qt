#!/usr/bin/env python
# -*- coding: utf-8 -*- 
import rospy
import collections
import math
import threading
import time
from lovot_internal_state.msg import *
from geometry_msgs.msg import Vector3

# class definitions
class CuriosityModel:
    u"""個別の好奇心クラス"""
    def __init__(self, name, initx, inity, windowsize):
        u"""初期化"""
        self.name = name
        self.x = initx
        self.y = inity
        self.uncertainity = 0.0
        self.existence = False
        self._extprob = 0.0
        self._windowsize = windowsize
        self._existence_array = collections.deque(maxlen=self._windowsize)

    def update_existence(self, curx, cury):
        u"""位置と存在の更新"""
        self.x = curx
        self.y = cury
        self.existence = True
            
    def update(self):
        u"""管理クラスから一定時間刻みで呼ばれるメソッド"""
        if len(self._existence_array) == self._windowsize:
            val = self._existence_array.popleft()
            self._extprob -= val / self._windowsize
        
        if self.existence:
            self._existence_array.append(1.0)
            self._extprob += 1.0 / self._windowsize
            self.existence = False
        else:
            self._existence_array.append(0.0)

        if self._extprob == 0.0:
            self.uncertainity = 0.0
        else:
            self.uncertainity = -1 * math.log(self._extprob, 2)
            if self.uncertainity < 0:
                self.uncertainity = 0.0
            
    u"""以下、アクセッサ"""
    def get_name(self):
        return self.name

    def get_existence(self):
        return self.existence    
    
    def get_location(self):
        return (self.x, self.y)

    def get_uncertainity(self):
        return self.uncertainity
        
class CuriosityManager:
    u"""好奇心管理クラス"""
    def __init__(self, name, updatetick, maxunit, curiosity_windowsize, unitdelete_thres, memorize_thres):
        u"""初期化"""
        self.name = name
        self._maxunitsize = maxunit
        self._update_tick = updatetick
        self._curiosity_windowsize = curiosity_windowsize
        self._unitdelete_thres = unitdelete_thres
        self._memorize_thres = memorize_thres
        self._unit_array = collections.deque(maxlen=self._maxunitsize)
        self._update_flag = True
        ipubname = '/lovot_curiosity/' + self.name + '/curiosity_list'
        self._curiosity_list_pub = rospy.Publisher(ipubname, CuriosityObjectList, queue_size=10)
        fpubname = '/lovot_curiosity/' + self.name + '/filtered_curiosity_list'
        self._filtered_curiosity_list_pub = rospy.Publisher(fpubname, CuriosityObjectList, queue_size=10)
        self._update_thread = threading.Thread(target=self.update, name="curiosity_update")
        self._update_thread.start()

    def __del__(self):
        u"""終了"""
        self._update_flag = False
        self._update_thread.join()

    def delete_nth_unit(self, n):
        u"""n番目の要素を削除する"""
        self._unit_array.rotate(-n)
        self._unit_array.popleft()
        self._unit_array.rotate(n)

    def delete_unit_by_name(self, name):
        u"""名前で指定したユニットを削除するメソッド"""
        cnt = 0
        for unit in self._unit_array:
            if name == unit.get_name():
                self.delete_nth_unit(cnt)
                return
            else:
                ctn += 1
        print 'No such unit!'

    def delete_lowest_uncertainity_unit(self):
        u"""最もuncertainityが低いユニットを一つ削除するメソッド"""        
        uncertainity_list = [x.get_uncertainity() for x in self._unit_array]
        minval = min(uncertainity_list)
        cnt = 0
        for unit in self._unit_array:
            if minval == unit.get_uncertainity():
                delete_nth_unit(cnt)
                return
            else:
                ctn += 1
        
    def update(self):
        u"""一定時間刻みで呼ばれるメソッド"""
        while self._update_flag:
            delete_units = []
            for unit in self._unit_array:
                if not unit.get_existence() and unit.get_uncertainity() < self._unitdelete_thres:
                    delete_units.append(unit.get_name())
                unit.update()

            for dname in delete_units:
                self.delete_unit_by_name(dname)
                
            time.sleep(self._update_tick)

    def publish_curiosity_list(self):
        u"""ソート済み好奇心リストをpublishするメソッド"""
        slist = sorted(self._unit_array, key=lambda x: x.get_uncertainity(), reverse=True)

        clist = CuriosityObjectList()
        fclist = CuriosityObjectList()
        for u in slist:
            cobj = CuriosityObject()
            cobj.name = u.get_name()
            cobj.location = Vector3()
            loc = u.get_location()
            cobj.location.x = loc[0]
            cobj.location.y = loc[1]
            cobj.location.z = 0.0
            cobj.uncertainity = u.get_uncertainity()
            clist.curiosity_objects.append(cobj)
            if u.get_uncertainity() < self._memorize_thres:
                fclist.curiosity_objects.append(cobj)
                
        if len(clist.curiosity_objects) > 0:
            # 瞬間的な値のpublish
            self._curiosity_list_pub.publish(clist)

        if len(fclist.curiosity_objects) > 0:            
            # ある程度出現しているものについてpublish
            self._filtered_curiosity_list_pub.publish(fclist)
        
    def update_object(self, name, posx, posy):
        u"""オブジェクトの更新"""
        unit_names = [x.get_name() for x in self._unit_array]
        
        if name in unit_names:
            unit = [x for x in self._unit_array if x.get_name() == name][0]
            unit.update_existence(posx, posy)
        else:
            if len(self._unit_array) == self._maxunitsize:
                self.delete_lowest_uncertainity_unit()
            self._unit_array.append(CuriosityModel(name, posx, posy, self._curiosity_windowsize))

        #ソート済みリストをpublishする
        self.publish_curiosity_list()

    def shutdown(self):
        self._update_flag = False
        self._update_thread.join()
